FROM python:3.6.10
RUN pip install pipenv
RUN mkdir -p /home/app
ENV APP_HOME=/home/app/web
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/static
RUN mkdir $APP_HOME/media
WORKDIR $APP_HOME
COPY Pipfile* $APP_HOME/
RUN pipenv lock --requirements > requirements.txt
RUN pip install -r requirements.txt
COPY . $APP_HOME
ENTRYPOINT ["/home/app/web/entrypoint.sh"]