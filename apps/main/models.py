from django.contrib.sites.models import Site
from django.core.mail import send_mail, get_connection, EmailMultiAlternatives
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.template.loader import get_template
from tinymce.models import HTMLField
from AvantaBackend import settings
from imagekit.models.fields import ProcessedImageField


class Theme(models.Model):
    name = models.CharField(max_length=15)

    def __str__(self):
        return str(self.name)


class Subscriber(models.Model):
    email = models.EmailField()
    created_at = models.DateTimeField(auto_now_add=True)
    theme = models.ManyToManyField(Theme)


class Content(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    main_image = ProcessedImageField(
        upload_to="main_images",
        format='JPEG',
        options={'quality': 35}
    )
    header = models.CharField(max_length=50)
    text = HTMLField()
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE)


@receiver(post_save, sender=Subscriber)
def save_welcome_email(sender, instance, **kwargs):
    send_mail(
        'Welcome',
        'Thank you for your subscription!',
        settings.EMAIL_HOST_USER,
        [instance.email],
        fail_silently=False,
    )


@receiver(post_save, sender=Content)
def save_newsletter_email(sender, instance, **kwargs):
    subs = Subscriber.objects.filter(theme=instance.theme).distinct()
    current_site = Site.objects.get_current().domain
    subs_array = []
    for sub in subs:
        subs_array.append(sub.email)
    letters = []
    for sub_email in subs_array:
        letter = (instance.header, instance.text, settings.EMAIL_HOST_USER, [sub_email], get_template('email.html').render({'instance': instance, 'site': current_site}))
        letters.append(letter)
    send_mass_html_mail(letters)


def send_mass_html_mail(datatuple, fail_silently=False, auth_user=None,
                        auth_password=None, connection=None):
    """
    Given a datatuple of (subject, message, html_message, from_email,
    recipient_list), send each message to each recipient list.
    Return the number of emails sent.
    If from_email is None, use the DEFAULT_FROM_EMAIL setting.
    If auth_user and auth_password are set, use them to log in.
    If auth_user is None, use the EMAIL_HOST_USER setting.
    If auth_password is None, use the EMAIL_HOST_PASSWORD setting.
    """
    connection = connection or get_connection(
        username=auth_user,
        password=auth_password,
        fail_silently=fail_silently,
    )
    messages = [
        EmailMultiAlternatives(subject, message, sender, recipient,
                               alternatives=[(html_message, 'text/html')],
                               connection=connection)
        for subject, message, sender, recipient, html_message,  in datatuple
    ]
    return connection.send_messages(messages)
