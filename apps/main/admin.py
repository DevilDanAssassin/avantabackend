from django.contrib import admin
# Register your models here.
from django_summernote.utils import get_attachment_model

from .models import Theme, Subscriber, Content
from django_summernote.admin import SummernoteModelAdmin


class ThemeAdmin(admin.ModelAdmin):
    list_display = ('name', )


class SubscribeAdmin(admin.ModelAdmin):
    list_display = ('email', 'created_at')


class ContentAdmin(SummernoteModelAdmin):
    list_display = ('header', 'theme', 'created_at')
    summernote_fields = ('text',)


admin.site.register(Theme, ThemeAdmin)
admin.site.register(Subscriber, SubscribeAdmin)
admin.site.register(Content, ContentAdmin)
