from rest_framework import serializers

from apps.main.models import Subscriber, Content, Theme


class ThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Theme
        fields = '__all__'


class SubscriberSerializer(serializers.ModelSerializer):
    theme = serializers.SlugRelatedField(slug_field='name',queryset=Theme.objects.all(), many=True)

    class Meta:
        model = Subscriber
        fields = '__all__'


class ContentDetailSerializer(serializers.ModelSerializer):
    theme = serializers.SlugRelatedField(slug_field='name',queryset=Theme.objects.all())

    class Meta:
        model = Content
        fields = '__all__'


