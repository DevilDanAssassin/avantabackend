from rest_framework import mixins, generics, viewsets
# Create your views here.
from apps.main.models import Subscriber, Content, Theme
from apps.main.serializers import SubscriberSerializer, ContentDetailSerializer, ThemeSerializer


class SubscriberView(mixins.CreateModelMixin,
                     generics.GenericAPIView):
    queryset = Subscriber.objects.all()
    serializer_class = SubscriberSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class SubscriberDetailView(mixins.RetrieveModelMixin,
                     generics.GenericAPIView):
    queryset = Subscriber.objects.all()
    serializer_class = SubscriberSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class ContentModelViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Content.objects.all()
    serializer_class = ContentDetailSerializer


class ThemeModelViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Theme.objects.all()
    serializer_class = ThemeSerializer